<?php

use App\Model\Gig\GigServiceType;
use Illuminate\Database\Seeder;

class GigServiceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $service_type = new GigServiceType();
    	$service_type->category_id = 5;
    	$service_type->title = "WordPress Website Development";
    	$service_type->description = "Develop Complete Website in WordPress.";
        $service_type->save();

        $service_type = new GigServiceType();
    	$service_type->category_id = 7;
    	$service_type->title = "Html Website";
    	$service_type->description = "Develop Complete Website in HTML.";
        $service_type->save();
    }
}
