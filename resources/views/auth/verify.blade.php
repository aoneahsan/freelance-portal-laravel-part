@extends('layouts.app')
@section('content')
@if(Session::has('code-send'))
<div class="alert alert-success">{{Session::get('code-send')}}</div>
@endif
@if(Session::has('code-send-fail'))
<div class="alert alert-danger">{{Session::get('code-send-fail')}}</div>
@endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verify Your Phone Number') }}</div>
                <div class="card-body">
                    @if (session('error'))
                    <div class="alert alert-danger" role="alert">
                        {{session('error')}}
                    </div>
                    @endif
                    Please enter the OTP sent to your number: {{session('phone_number')}}
                    <form action="{{route('verify')}}" method="post">
                        @csrf
                        <div class="form-group row">
                            <label for="verification_code"
                                class="col-md-4 col-form-label text-md-right">{{ __('OTP: ') }}</label>
                            <div class="col-md-6">
                                <input id="verification_code" type="tel"
                                    class="form-control @error('verification_code') is-invalid @enderror"
                                    name="verification_code" value="{{ old('verification_code') }}" required>
                                @error('verification_code')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Verify') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                @if(Auth::user())
                <hr>
                <div class="form-group">
                    <div class="col-md-12 col-md-offset-4">
                        <form action="{{ url('resend-verify-code') }}" method="post">
                            @csrf
                        <input type="hidden" name="user_id" value="{{ Auth::user()->authy_id }}">
                            <button type="submit" class="btn btn-success">Resend Code</button>
                        </form>
                        <p>kindly wait at least for 30 seconds (this is only accessable 1 time in one minute)</p>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection