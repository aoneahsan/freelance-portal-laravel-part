<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, HasRoles, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'username', 'name', 'email', 'password', 'phone_number', 'country_code', 'profile_img', 'role', 'is_buyer', 'authy_id', 'is_2fa_verified', 'is_2fa_enabled', 'seller_plan_id', 'buyer_plan_id'
    // ];

    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime'
    ];

    public function getProfileImg()
    {
        if (!!$this->profile_img) {
            return Storage::url($this->profile_img);
        } else {
            return 'assets/img/placeholder.jpg';
        }
    }

    public function account()
    {
        return $this->hasOne('App\Model\User\UserAccount');
    }

    public function details()
    {
        return $this->hasOne('App\Model\User\UserDetails');
    }

    public function securityQuestions()
    {
        return $this->hasMany('App\Model\User\UserSecurityQuestion');
    }

    public function getTokken()
    {
        // return $this->createToken($request->device_name)->plainTextToken;
        return $this->createToken('mobile')->plainTextToken;
    }
    
    public function identities()
    {
        return $this->hasMany('App\Model\App\SocialIdentity');
    }

    public function gigs()
    {
        return $this->hasMany('App\Model\Gig\UserGig', 'user_id', 'id');
    }

    public function sellerplandetails()
    {
        return $this->hasOne('App\Model\Shared\MembershipPlan', 'id', 'seller_plan_id');
    }

    public function buyerplandetails()
    {
        return $this->hasOne('App\Model\Shared\MembershipPlan', 'id', 'buyer_plan_id');
    }
}
