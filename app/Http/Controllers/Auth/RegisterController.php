<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\User;
use App\Model\User\UserDetails;
use App\Model\User\UserAccount;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Authy\AuthyApi as AuthyApiFacade;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'confirmed'],
            'phone_number' => ['required', 'string', 'unique:users'],
            'role' => ['required'],
            'country_code' => 'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $authy_api = new AuthyApiFacade(env('AUTHY_SECRET'));
        $authy_user = $authy_api->registerUser($data['email'], $data['phone_number'], $data['country_code']);
        if ($authy_user) {
            $authy_id = $authy_user->id();
            $user =  User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'phone_number' => $data['phone_number'],
                'country_code' => $data['country_code'],
                'role' => $data['role'],
                'authy_id' => $authy_id
            ]);
    
            UserDetails::create([
                'user_id' => $user->id
            ]);

            UserAccount::create([
                'user_id' => $user->id
            ]);

            $user->assignRole($data['role']);
    
            return $user;
            
        } else {
            dd("Error Occured, While Registering");
        }
        
    }
}
