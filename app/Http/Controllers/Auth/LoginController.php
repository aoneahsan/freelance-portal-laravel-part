<?php

namespace App\Http\Controllers\Auth;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

// Models
Use App\User;
use App\Model\App\SocialIdentity;
use App\Model\User\UserAccount;
use App\Model\User\UserDetails;
// Facades
use Authy\AuthyApi;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // Sociallite Package Code
    public function redirectToProvider($provider)
   {
       return Socialite::driver($provider)->redirect();
   }

   public function handleProviderCallback($provider)
   {
       try {
           $user = Socialite::driver($provider)->user();
        } catch (Exception $e) {
            return redirect('/login');
        }

       $authUser = $this->findOrCreateUser($user, $provider);
    //    Auth::login($authUser, true);
       $token = $authUser->getTokken();
       $user_id = $authUser->id;

    //    dd(env('APP_FRONTEND_URL').'sociallogin?user_id='.$user_id.'&token='.$token);
       return redirect()->to(env('APP_FRONTEND_URL').'sociallogin?user_id='.$user_id.'&token='.$token);
   }

   public function findOrCreateUser($providerUser, $provider)
   {
       $account = SocialIdentity::whereProviderName($provider)
                  ->whereProviderId($providerUser->getId())
                  ->first();

       if ($account) {
            \session(['isVerified' => true]);
           return $account->user;
       } else {
           $user = User::whereEmail($providerUser->getEmail())->first();

           if (! $user) {
               $user = User::create([
                   'email' => $providerUser->getEmail(),
                   'name'  => $providerUser->getName(),
                   'role' => 'seller'
               ]);
                UserDetails::create([
                    'user_id' => $user->id
                ]);
                UserAccount::create([
                    'user_id' => $user->id
                ]);
           }

           $user->identities()->create([
               'provider_id'   => $providerUser->getId(),
               'provider_name' => $provider
           ]);

           \session(['isVerified' => true]);
           return $user;
       }
   }

    // Authy 2FA AUTH
    protected function authenticated(Request $request, $user)
    {
        if ($user->is_2fa_enabled) {
            $authy_api = new AuthyApi(env('AUTHY_SECRET'));
            $authy_api->requestSms($user->authy_id);
            \session(['isVerified' => false]);
            return \redirect('verify');
        }
        else {
            \session(['isVerified' => true]);
        }
    }
}
